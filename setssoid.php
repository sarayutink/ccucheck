<?php
// side effect
ini_set('error_reporting', E_ERROR);

// declaration
define("RDHOST", "bzrdhost");
define("RDPORT", 6379);
define("RDDB", 3);

define("LOGDIR", __DIR__ ."/logs");

function main ($json) {
  $params = json_decode($json, true);
  $compressed = base64_encode(gzdeflate($json));
  $logsparam = array(date("Y:m:d-H:i:s"), RDHOST.":".RDPORT."[".RDDB."]", $json);
  // writeLogs("param", $json ."\n", FILE_APPEND);

  try {
    if (setexRedis($params)) {
      writeLogs("access", $logsparam);
      echo "SUCCESS";
    }
  }
  catch (Exception $e) {
      $logsparam[] = $e->getMessage();
      writeLogs("error", $logsparam);
      notify("error", $logsparam);
      newrelic_notice_error($e);
      echo "FAIL";
  }
}

function setexRedis ($params) {
  $redis = new Redis();
  $redis->connect(RDHOST, RDPORT);
  $redis->select(RDDB);
  $redis->multi();
  foreach ($params as $row) 
    foreach ($row['body'] as $body) 
      if (!is_null($body)) $redis->setex($body['device'], $row['ttl'], $row['value']);
  $return =$redis->exec();
  $redis->close();

  return $return;
}

function writeLogs ($mode, $details) {
  $timestamp = date("Y:m:d-H:i:s");
  if (is_array($details)) $details = implode("  ", $details);
  $details = $timestamp . $details;
  file_put_contents(LOGDIR."/setssoid_".date("Ymd_H")."_".$mode.".log", $details ."\n", FILE_APPEND);
}

function notify ($mode, $details) {
  date_default_timezone_set("Asia/Bangkok");

  $sToken = "Xm4z8rZ4Rn2Y6nqAcuouo68V9o2F8uFiPW87Yle2Jgy";

  $timestamp = date("Y:m:d-H:i:s");
  if (is_array($details)) $details = implode("  ", $details);
  $details = $timestamp . $details;

  $chOne = curl_init(); 
  curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify"); 
  curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0); 
  curl_setopt( $chOne, CURLOPT_PROXY, "10.18.19.151:80");
  curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0); 
  curl_setopt( $chOne, CURLOPT_POST, 1); 
  curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=".$details); 
  $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$sToken.'', );
  curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers); 
  curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1); 
  $result = curl_exec( $chOne ); 

  //Result error 
  if(curl_error($chOne)) file_put_contents(LOGDIR."/line-notify-err_".date("Ymd_H").".log", date("Y/m/d H:i:s")."  ".'error:' . curl_error($chOne)."\n", FILE_APPEND);
  curl_close( $chOne );
}

## POST JSON body
$json = file_get_contents("php://input");

main($json);
?>
