<?php
// side effect
ini_set('error_reporting', E_ERROR);

// declaration
define("RDHOST", "10.18.19.102");
define("RDPORT", 6379);
define("RDDB", 3);

define("LOGDIR", __DIR__ ."/logs");

function main ($json) {
  $params = json_decode($json, true);
  $compressed = base64_encode(gzdeflate($json));
  $logsparam = array(date("Y:m:d-H:i:s"), RDHOST.":".RDPORT."[".RDDB."]", $json);
  // writeLogs("param", $json ."\n", FILE_APPEND);

  try {
    if (setexRedis($params)) {
      writeLogs("access", $logsparam);
      echo "SUCCESS";
    }
  }
  catch (Exception $e) {
      $logsparam[] = $e->getMessage();
      writeLogs("error", $logsparam);
      newrelic_notice_error($e);
      echo "FAIL";
  }
}

function setexRedis ($params) {
  $redis = new Redis();
  $redis->connect(RDHOST, RDPORT);
  $redis->select(RDDB);
  $redis->multi();
  foreach ($params as $row) 
    foreach ($row['body'] as $body) 
      if (!is_null($body)) $redis->setex($body['device'], $row['ttl'], $row['value']);
  $return =$redis->exec();
  $redis->close();

  return $return;
}

function writeLogs ($mode, $details) {
  $timestamp = date("Y:m:d-H:i:s");
  if (is_array($details)) $details = implode("  ", $details);
  $details = $timestamp . $details;
  file_put_contents(LOGDIR."/stg_setssoid_".date("Ymd_H")."_".$mode.".log", $details ."\n", FILE_APPEND);
}



## mockup JSON
// $json = '
// [
//   {
//     "ssoid" : "ssoid1",
//     "value": "utc",
//     "ttl" : 120,
//     "body": [
//       {
//         "device": "ssoid1|device1|UA1|ht112"
//       },
//       {
//         "device": "ssoid1|device2|UA2|ht111"
//       }
//     ]
//   },
//   {
//     "key" : "ssoid2",
//     "value": "utc",
//     "ttl" : 120,
//     "body": [
//       {
//         "device": "ssoid2|device1|UA1|ht112"
//       },
//       {
//         "device": "ssoid2|device2|UA2|ht111"
//       }
//     ]
//   }
// ]';

## make 5000 array elements
// $json_arr = array();
// for ($i=0; $i<5000; ++$i) {
//   $array = array("ssoid" => "ssoid_n", "value" => date(), "ttl" => 120, "body" => array(array("device" => "ssoid_n|device_id_1|UA_1|CHID"), array("device" => "ssoid_n|device_id_2|UA_2|CHID")));
//   $array["ssoid"] = str_replace("_n", "_{$i}", $array["ssoid"]);
//   $array["body"][0] = str_replace("_n", "_{$i}", $array["body"][0]);
//   $array["body"][1] = str_replace("_n", "_{$i}", $array["body"][1]);

//   $json_arr[] = $array;
// }
// $json = json_encode($json_arr);

## POST JSON body
$json = file_get_contents("php://input");

main($json);
?>